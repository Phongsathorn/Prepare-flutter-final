import 'package:flutter/material.dart';
import 'screens/login.dart';
import 'screens/register.dart';
import 'screens/home.dart';
import 'screens/profileSetup.dart';
import 'screens/friend.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: myColor,
      ),
      debugShowCheckedModeBanner: false,
      routes: {
        '/': (context) => LoginScreen(),
        '/home': (context) => HomeScreen(),
        '/register': (context) => RegisterScreen(),
        '/setup': (context) => ProfileSetupScreen(),
        '/friend': (context) => FriendsScreen()
      },
      initialRoute: '/',
    );
  }
}

const MaterialColor myColor = const MaterialColor(0xff303f9f, const <int, Color>{
  50: const Color(0xff303f9f),
  100: const Color(0xff303f9f),
  200: const Color(0xff303f9f),
  300: const Color(0xff303f9f),
  400: const Color(0xff303f9f),
  500: const Color(0xff303f9f),
  600: const Color(0xff303f9f),
  700: const Color(0xff303f9f),
  800: const Color(0xff303f9f),
  900: const Color(0xff303f9f)
});
