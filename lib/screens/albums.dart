import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'photos.dart';

class AlbumsScreen extends StatelessWidget{
  final int id;

  const AlbumsScreen({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Albums'),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            width: double.infinity,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: RaisedButton(
                child: Text('Back'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ),
          ),
          FutureBuilder(
            future: this.fetchAlbums(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData){
                return Expanded(
                  child: ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      Album album = snapshot.data[index];
                      return Container(
                        padding: EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                        child: Card(
                          child: InkWell(
                            child: Container(
                              padding: EdgeInsets.all(10),
                              child: ListTile(
                                title: Text('${album.id}'),
                                subtitle: Text('${album.title}'),
                              ),
                            ),
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) {
                                return PhotosScreen(id: album.id);
                              }));
                            },
                          ),
                        ),
                      );
                    },
                  )
                );
              } else if (snapshot.hasError){
                return Text('${snapshot.error}');
              }
              return CircularProgressIndicator();
            },
          )
        ]
      ),
    );
  }

  Future<List<Album>> fetchAlbums() async {
    final response = await http.get('https://jsonplaceholder.typicode.com/users/$id/albums');
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      List<Album> albums = json.decode(response.body).map<Album>((post) => Album.fromJson(post)).toList();
      return albums;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load posts');
    }
  }
}

class Album{
  final int id;
  final String title;

  Album({this.id, this.title});

  factory Album.fromJson(Map<String, dynamic> json){
    return Album(
      id: json['id'],
      title: json['title']
    );
  }
}