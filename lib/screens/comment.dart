import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_final/screens/post.dart';

import 'package:http/http.dart' as http;

class CommentScreen extends StatelessWidget{
  final Post post;

  const CommentScreen({Key key, this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('comment'),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            width: double.infinity,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: RaisedButton(
                child: Text('Back'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ),
          ),
          FutureBuilder(
              future: this.fetchComment(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData){
                  return Expanded(
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        Comment comment = snapshot.data[index];
                        return Container(
                          padding: EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                          child: Card(
                            child: Container(
                              padding: EdgeInsets.all(10),
                              child: ListTile(
                                title: Text('${this.post.id} : ${comment.id}'),
                                subtitle: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('${comment.body}\n'),
                                    Text('${comment.name}'),
                                    Text('(${comment.email})')
                                  ],
                                ),
                              ),
                            ),
                          ),
                        );
                      }
                    )
                  );
                }
                else if (snapshot.hasError){
                  return Text('${snapshot.error}');
                }
                return CircularProgressIndicator();
              },
            ),
        ],
      ),
    );
  }

  Future<List<Comment>> fetchComment() async {
    final response = await http.get('https://jsonplaceholder.typicode.com/posts/${this.post.id}/comments');
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      List<Comment> comments = json.decode(response.body).map<Comment>((comment) => Comment.fromJson(comment)).toList();
      return comments;
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load comments');
    }
  }
}

class Comment {
  final int id;
  final String body;
  final String name;
  final String email;

  Comment({this.id, this.body, this.name, this.email});

  factory Comment.fromJson(Map<String, dynamic> json){
    return Comment(
      id: json['id'],
      body: json['body'],
      name: json['name'],
      email: json['email']
    );
  }
}